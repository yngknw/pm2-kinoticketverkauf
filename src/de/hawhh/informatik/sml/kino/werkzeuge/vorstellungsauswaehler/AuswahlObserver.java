package de.hawhh.informatik.sml.kino.werkzeuge.vorstellungsauswaehler;

import javax.swing.JButton;

import de.hawhh.informatik.sml.kino.werkzeuge.SubwerkzeugObserver;

public interface AuswahlObserver extends SubwerkzeugObserver
{
	void reagiereAufAenderung(JButton button);
}
