package de.hawhh.informatik.sml.kino.werkzeuge;


import de.hawhh.informatik.sml.kino.werkzeuge.platzverkauf.JPlatzButton;
import de.hawhh.informatik.sml.kino.werkzeuge.platzverkauf.JPlatzplan;

/**
 * Interface für Kontextwerkzeuge, die Subwerkzeuge beobachten möchten.
 * 
 * Werkzeuge, die dieses Interface implementieren, können sich an einem
 * Subwerkzeug, das von {@link ObservableSubwerkzeug} erbt, als Beobachter
 * registrieren. Sie werden dann vom Subwerkzeug bei Änderungen benachrichtigt,
 * zum Beispiel bei der Auswahl eines Materials in einer Liste durch den
 * Benutzer.
 * 
 * Damit ein Kontextwerkzeug mehrere Subwerkzeuge beobachten und auf deren
 * Nachrichten unterschiedlich reagieren kann, bietet es sich an, dieses
 * Interface in inneren Klassen (inner classes) zu implementieren.
 * 
 * @author SE2-Team (Uni HH), PM2-Team
 * @version SoSe 2017
 */
public interface SubwerkzeugObserver
{
    /**
     * Reagiert auf eine Änderung in dem beobachteten Subwerkzeug.
     */
    void reagiereAufAenderung();
    //FIXME neue Methode von SubwerkzeugObserver die auf einen Buttonklick reagiert
    //übergeben werden der Button der geklickt wurde, der Platzplan auf dem der 
    //Button liegt. blockieren gibt an ob der Platz markiert oder demarkiert wurde.
    //true wenn markiert, false wenn demarkiert
    void reagiereAufButtonklick(JPlatzplan platzplan, JPlatzButton button, boolean blockieren);
}
