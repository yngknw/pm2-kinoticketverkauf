package de.hawhh.informatik.sml.kino.werkzeuge.platzverkauf.barzahlung;


import java.awt.Dialog;
import java.awt.Dialog.ModalityType;
import java.awt.Window;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;


/**
 * Die UI des {@link Bezahlwerkzeug}.
 * 
 * @author Torben Kunow, Ömer Kirdas
 * @version SoSe 2017
 */
public class BezahlwerkzeugUI
{
	private JDialog _dialog;
	private JTextField _eingabefeld;
	private JButton _okButton;
	private JButton _abbruchButton;
	private JLabel _gueltigeEingabeHinweis;
	private JLabel _euroZeichen;
	private JLabel _gesamtpreisWort;
	private JLabel _bisherGezahltWort;
	private JLabel _rueckgeldWort;

	private JLabel _gesamtpreis;
	private JLabel _bisherGezahlt;
	private JLabel _rueckgeld;

	/**
	 * Erzeugt Ein Frame, in dem die Barzahlungsoberfläche mit dem Eingabefeld,
	 * Buttons und Informationen über den Gesamtbetrag, dem Rückgeld und dem
	 * bereits bezahlten Betrag, darstellt wird.
	 */
	public BezahlwerkzeugUI(Window parentFrame)
	{
		_dialog = new JDialog(parentFrame, "Barzahlung", Dialog.ModalityType.DOCUMENT_MODAL);
		_okButton = new JButton("OK");
		_abbruchButton = new JButton("Abbrechen");
		_eingabefeld = new JTextField(15);
		_gesamtpreisWort = new JLabel("Gesamtpreis:");
		_bisherGezahltWort = new JLabel("Gezahlt:");
		_rueckgeldWort = new JLabel("Wechselgeld:");
		_gueltigeEingabeHinweis = new JLabel("z.B.: 123,45€");
		_euroZeichen = new JLabel("€");
		_gesamtpreis = new JLabel();
		_bisherGezahlt = new JLabel("0,00€");
		_rueckgeld = new JLabel("0,00€");

		_okButton.setEnabled(false);

		_gesamtpreis.setBounds(330, 15, 200, 30);
		_gueltigeEingabeHinweis.setBounds(40, 15, 200, 30);
		_gesamtpreisWort.setBounds(240, 15, 200, 30);

		_bisherGezahltWort.setBounds(240, 45, 200, 30);
		_bisherGezahlt.setBounds(330, 45, 200, 30);
		

		_eingabefeld.setBounds(40, 45, 160, 30);
	
	    
	    
		_euroZeichen.setBounds(205, 45, 20, 30);

		_okButton.setBounds(30, 85, 60, 30);
		_rueckgeldWort.setBounds(240, 85, 200, 30);
		_rueckgeld.setBounds(330, 85, 200, 30);
		_abbruchButton.setBounds(110, 85, 100, 30);

		_dialog.setLayout(null);
		_dialog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		_dialog.setResizable(false);
		_dialog.setSize(450, 160);
		_dialog.setLocationRelativeTo(parentFrame);

		_dialog.add(_rueckgeld);
		_dialog.add(_gesamtpreis);
		_dialog.add(_bisherGezahlt);
		_dialog.add(_euroZeichen);
		_dialog.add(_gueltigeEingabeHinweis);
		_dialog.add(_gesamtpreisWort);
		_dialog.add(_abbruchButton);
		_dialog.add(_okButton);
		_dialog.add(_eingabefeld);
		_dialog.add(_bisherGezahltWort);
		_dialog.add(_rueckgeldWort);

		_dialog.setModalityType(ModalityType.DOCUMENT_MODAL);

	}

	/**
	 * gibt den "OK" Button zurück.
	 * 
	 * @return Ein "OK" Button
	 * 
	 */
	public JButton getOkButton()
	{
		return _okButton;
	}

	/**
	 * gibt den "Abbrechen" Button zurück
	 * 
	 * @return Ein "Abbrechen" Button
	 */
	public JButton getAbbruchButton()
	{
		return _abbruchButton;
	}

	/**
	 * Gibt das Label zurück, in dem der Gesamtpreis dargestellt wird.
	 * 
	 * @return Der Gesamtpreis
	 */
	public JLabel getGesamtpreisLabel()
	{
		return _gesamtpreis;
	}

	/**
	 * Gibt das Label zurück, in dem das Rückgeld dargestellt wird.
	 * 
	 * @return Das Rückgeld
	 */
	public JLabel getRueckgeld()
	{
		return _rueckgeld;
	}

	/**
	 * Gibt das Label zurück, in dem der bereits bezahlte Betrag dargestellt
	 * wird.
	 * 
	 * @return
	 */
	public JLabel getBisherGezahlt()
	{
		return _bisherGezahlt;
	}

	/**
	 * Gibt das Textfeld zurück, in dem man einen Betrag tippen kann.
	 * 
	 * @return Ein Textfeld
	 */
	public JTextField getTextfeld()
	{
		return _eingabefeld;
	}

	/**
	 * gibt das Label zurück, in dem die Informationen über eine gültige Eingabe
	 * beschrieben ist.
	 * 
	 * @return Die Gueltige Eingabe.
	 */
	public JLabel getGueltigerEingabeHinweis()
	{
		return _gueltigeEingabeHinweis;
	}

	/**
	 * gibt einen Dilaog zurück
	 * 
	 * @return Ein Dialog
	 */
	public JDialog getDialog()
	{
		return _dialog;
	}
}
