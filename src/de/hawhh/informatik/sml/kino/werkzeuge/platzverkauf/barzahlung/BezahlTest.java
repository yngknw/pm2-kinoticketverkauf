//package de.hawhh.informatik.sml.kino.werkzeuge.platzverkauf.barzahlung;
//
//import static org.junit.Assert.assertFalse;
//import static org.junit.Assert.assertTrue;
//
//import org.junit.Test;
//
//public class BezahlTest
//{
//	private Bezahlservice _b;
//	
//	public BezahlTest()
//	{
//		_b = new Bezahlservice(1000);
//	}
//	@Test
//	public void testIstGueltigeEingabe()
//	{
//		assertTrue(_b.istGueltigeEingabe("123,12"));
//		assertTrue(_b.istGueltigeEingabe("65,12"));
//		assertFalse(_b.istGueltigeEingabe("0.00"));
//		assertFalse(_b.istGueltigeEingabe("111"));
//		assertFalse((_b.istGueltigeEingabe("00")));
//		assertFalse((_b.istGueltigeEingabe("1,000")));
//		assertFalse((_b.istGueltigeEingabe("1,90234")));
//		assertFalse((_b.istGueltigeEingabe("0,000")));
//		assertFalse((_b.istGueltigeEingabe("1,90234")));
//		assertFalse(_b.istGueltigeEingabe(","));
//	}
//	@Test
//	public void testStringToInt()
//	{
//		
//		assertTrue(12==_b.stringToInt("0.12"));
//		assertTrue(0 == _b.stringToInt("0.00"));
//		assertTrue(1908 == _b.stringToInt("19.08"));
//		assertTrue(23948 == _b.stringToInt("239.48"));
//	}
//}
