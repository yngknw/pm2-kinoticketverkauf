package de.hawhh.informatik.sml.kino.werkzeuge.platzverkauf.barzahlung;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;


import de.hawhh.informatik.sml.kino.fachwerte.Geldbetrag;

/**
 * Mit diesem Werkzeug können Plätze bezahlt werden.
 * 
 * @author Torben Kunow, Ömer Kirdas
 * @version SoSe 2017
 */
public class Bezahlwerkzeug
{
    private boolean _istVerkauft;
    private BezahlwerkzeugUI _ui;
    private Bezahlservice _bezahlservice;

    /**
     * Initialisiert das Bezahlwerkzeug
     * 
     * @param plaetze
     *            Die Plätze, welche verkauft werden sollen.
     * @param vorstellung
     *            Die Vorstellung, in dem die Plätze verkauft werden sollen.
     */
    public Bezahlwerkzeug(Geldbetrag gesamtpreis, Window parentFrame)
    {
        _ui = new BezahlwerkzeugUI(parentFrame);
        _istVerkauft = false;
        _bezahlservice = new Bezahlservice(gesamtpreis);
        _ui.getGesamtpreisLabel().setText(_bezahlservice.getGesamtpreisString() + "€");
        registriereListener();
        _ui.getDialog().setVisible(true);
    }

    /**
     * Fügt der UI die Funktionalität hinzu mit entsprechenden Listenern.
     */
    public void registriereListener()
    {
        // Jede Änderung des Textfeldes wird registriert
        _ui.getTextfeld().getDocument().addDocumentListener(new DocumentListener()
        {
            
            @Override
            public void removeUpdate(DocumentEvent e)
            {
                changedUpdate(e);
            }

            @Override
            public void insertUpdate(DocumentEvent e)
            {
                changedUpdate(e);
            }

            // Wenn die Eingabe des Geldbetrages gültig ist(z.B. 1.23) wird der
            // OkButton drückbar
            @Override
            public void changedUpdate(DocumentEvent e)
            {
                String eingabe = _ui.getTextfeld().getText();
                
                
                    if (_bezahlservice.getGesamtpreis().istGueltigeEingabe(eingabe)) // ToDos
                    {
                        Geldbetrag geldbetragEingabe = _bezahlservice.getGesamtpreis().StringToGeldbetrag(eingabe);
                        _ui.getBisherGezahlt().setText(eingabe + "€");

                        if (_bezahlservice.getGesamtpreis().compare(geldbetragEingabe) <= 0) // ToDos
                        {
                            _ui.getRueckgeld().setText(
                                    geldbetragEingabe.subGeldbetraege(_bezahlservice.getGesamtpreis()).toString()
                                            + "€"); // ToDos
                            _ui.getOkButton().setEnabled(true);
                        }
                        else
                        {
                            _ui.getOkButton().setEnabled(false);
                            _ui.getRueckgeld().setText("0,00" + "€");
                        }
                    }
                    else
                    {
                        _ui.getBisherGezahlt().setText("Ungueltige Eingabe!");
                        _ui.getRueckgeld().setText("0,00€");
                        _ui.getOkButton().setEnabled(false);
                    }
                
            }
        });

        // Wenn der AbbruchButton gedrückt wird schließt sich das Fenster
        _ui.getAbbruchButton().addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                _istVerkauft = false;
                _ui.getDialog().dispose();
            }
        });
        // OkButton wenn der Button gedrückt wird werden die Plätze verkauft und
        // das Fenster geschlossen
        _ui.getOkButton().addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                _istVerkauft = true;
                _ui.getDialog().dispose();
            }

        });

    }

    public boolean getIstVerkauft()
    {
        return _istVerkauft;
    }

}
