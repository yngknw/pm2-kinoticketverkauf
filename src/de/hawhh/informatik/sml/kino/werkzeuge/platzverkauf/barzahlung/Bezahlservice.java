package de.hawhh.informatik.sml.kino.werkzeuge.platzverkauf.barzahlung;

import de.hawhh.informatik.sml.kino.fachwerte.Geldbetrag;

/**
 * Ein Bezahl-Service ist ein Service, der alle zur Verfügung stehenden Beträge
 * enthält.
 * 
 * @author Torben Kunow, Ömer Kirdas
 * @version SoSe 2017
 */
public class Bezahlservice
{
	private Geldbetrag _gesamtpreis; // ToDos
	private Geldbetrag _rueckgeld; // ToDos
	private Geldbetrag _bezahlterBetrag; // ToDos

	public Bezahlservice(Geldbetrag gesamtpreis) // ToDos
	{
		_gesamtpreis = gesamtpreis;
		_rueckgeld = Geldbetrag.get(0);
		_bezahlterBetrag = Geldbetrag.get(0);
	}

	/**
	 * Gibt diesen Geldbetrag in der Form "10,21" zurück.
	 * 
	 * @return result Geldbetrag in Form eines Strings
	 */
	public String getGesamtpreisString()
	{
		return _gesamtpreis.toString();
	}

	/**
	 * liefert den Gesamtpreis für die KinoKarten
	 * 
	 * 
	 * @return _gesamtpreis Der Gesamtpreis in Eurocent
	 */
	public Geldbetrag getGesamtpreis() // ToDos
	{
		return _gesamtpreis;
	}

	/**
	 * 
	 * liefert das Rückgeld
	 * 
	 * @return _rueckgeld Das Rückgeld in Eurocent
	 */
	public Geldbetrag getRueckgeld() // ToDos
	{
		return _rueckgeld;
	}

	/**
	 * 
	 * liefert den bereits bezahlten Betrag.
	 * 
	 * @return _bezahlterBetrag Der schon bezahlte Betrag in Eurocent.
	 */
	public Geldbetrag getBezahlterBetrag() // ToDos
	{
		return _bezahlterBetrag;
	}

	// ToDos
	// /**
	// * Von String(In der Form zahl.zahlmit2ziffern) zu int in eurocent "12,23"
	// -> 1223
	// *
	// * @require s != null
	// *
	// * @param s
	// *
	// * @return result Eine Ganze Zahl
	// */
	// public int stringToInt(String s)
	// {
	// assert s != null : "Vorbedingung verletzt: s != null";
	//
	// int result = 0;
	// for(int i = 0; i < s.length(); i++)
	// {
	// if(i != s.length()-3)
	// {
	// result = result*10;
	// result = (result + s.charAt(i) - 48);
	// }
	//
	// }
	// return result;
	// }
	//
	// /**
	// * Wandelt einen int in eurocent in einen String um. z.B. 1223 -> "12,23"
	// * @param i
	// * @return Eine Zahl als String
	// */
	// public String intToString(int i)
	// {
	// String letzteZiffer= "" + i%10;
	// String vorletzteZiffer = "" + (i % 100)/10;
	//
	// return i/100 + "," + vorletzteZiffer + letzteZiffer;
	// }
	//
	// /**
	// *
	// * @param s1
	// * @param s2
	// * @return
	// */
	// public String addGeldbetraege(String s1, String s2)
	// {
	// return intToString(stringToInt(s1) + stringToInt(s2));
	// }
	//
	// /**
	// *
	// * @param minuend
	// * @param subtrahend
	// * @return
	// */
	// public String subGeldbetraege(String minuend, String subtrahend)
	// {
	// return intToString(stringToInt(minuend) - stringToInt(subtrahend));
	// }
	//
	//
	// /**
	// * Gibt an ob ein String eine gültige Eingabe ist. Eine gültige Eingabe
	// hat die
	// * Form zahl.zahlmit2ziffern z.B. 100.12
	// */
	// public boolean istGueltigeEingabe(String s)
	// {
	// boolean gueltig = true;
	// boolean vorKomma = true;
	// boolean ersteZahlNull;
	//
	// if(s.length() >= 4)
	// {
	// ersteZahlNull = s.charAt(0) == '0';
	// if(s.charAt(s.length()-3) != ',' || ersteZahlNull && s.charAt(1) != ',')
	// {
	// gueltig = false;
	// }
	//
	// for(int i = 0; i < s.length() && gueltig; i++)
	// {
	// if(s.charAt(i) == ',')
	// {
	// vorKomma = false;
	// }
	//
	// if((s.charAt(i) < '0' || s.charAt(i) > '9') && s.charAt(i) != ',')
	// {
	// gueltig = false;
	// }
	// else if(ersteZahlNull && vorKomma && s.charAt(i) != '0')
	// {
	// gueltig = false;
	// }
	// else if((s.charAt(i) == ',') && (s.length() - i > 3))
	// {
	// gueltig = false;
	// }
	// }
	// }
	// else
	// {
	// gueltig = false;
	// }
	// return gueltig;
	//
	// }
}
