package de.hawhh.informatik.sml.kino.startup;


import de.hawhh.informatik.sml.kino.werkzeuge.ObservableSubwerkzeug;
import de.hawhh.informatik.sml.kino.werkzeuge.platzverkauf.JPlatzButton;
import de.hawhh.informatik.sml.kino.werkzeuge.platzverkauf.JPlatzplan;

/**
 * Ein Service zuständig für die Kommunikation der Kassen untereinander.
 * 
 * 
 * @author Torben Kunow, Ömer Kirdas
 *
 */
//FIXME neuer Service erbt von ObservableSubwerkzeug 
public class BenachrichtigungsService extends ObservableSubwerkzeug
{
	/**
	 * 
	 * Informiert Kassen über einen geklickten Button. Blockiert oder endblockiert dementsprechend die Plätze an den anderen Kassen.
	 * 
	 * @param platzplan Der Platzplan an dem der Button gedrückt wurde
	 * @param button der Button der gedrückt wurde
	 * @param blockieren true wenn die anderen Plätze blockiert werden sollen, false sonst.
	 */
	public synchronized void registriereButtonAktion(JPlatzplan platzplan, JPlatzButton button, boolean blockieren)
	{
		informiereUeberButton(platzplan, button, blockieren);
	}
	
	
	/**
	 * Informiert bei einem Verkauf, einer Stornierung oder der Auswahl einer neuen 
	 * Vorstellung die anderen Kassen darüber dass sie ihren Platzplan aktualisieren müssen.
	 */
	public void registriereAenderung()
	{
		informiereUeberAenderung();
	}
	
}
