package de.hawhh.informatik.sml.kino.fachwerte;

//import java.math.BigInteger;

/**
 * Ein Geldbetrag in Euro, gespeichert als ganze Euro- und ganze Cent-Beträge.
 * 
 * @author SE2-Team, PM2-Team
 * @version SoSe 2016
 */
public final class Geldbetrag
{

	private final int _euroAnteil;
	private final int _centAnteil;

	/**
	 * Ein Selektor erzeugt einen neuen Geldbetrag
	 * 
	 * @require eurocent >= 0;
	 * 
	 * @param eurocent
	 *            Der Betrag in ganzen Euro-Cent
	 * @return Ein Geldbetrag
	 */
	public static Geldbetrag get(int eurocent)
	{
		assert eurocent >= 0 : "Vorbedingung verletzt: eurocent >= 0";

		return new Geldbetrag(eurocent);
	}

	/**
	 * Wählt einen Geldbetrag aus.
	 * 
	 * @param eurocent
	 *            Der Betrag in ganzen Euro-Cent
	 * 
	 * 
	 */
	private Geldbetrag(int eurocent)
	{

		_euroAnteil = eurocent / 100;
		_centAnteil = eurocent % 100;

	}

	/**
	 * Gibt den Eurobetrag ohne Cent zurück.
	 * 
	 * @return Den Eurobetrag ohne Cent.
	 */
	public int getEuroAnteil()
	{
		return _euroAnteil;
	}

	/**
	 * Gibt den Centbetrag ohne Eurobetrag zurück.
	 */
	public int getCentAnteil()
	{
		return _centAnteil;
	}

	/**
	 * Liefert einen formatierten String des Geldbetrags in der Form "10,23"
	 * zurück.
	 * 
	 * @return eine String-Repräsentation.
	 */
	public String getFormatiertenString()
	{
		return _euroAnteil + "," + getFormatiertenCentAnteil();
	}

	/**
	 * Liefert einen zweistelligen Centbetrag zurück.
	 * 
	 * @return eine String-Repräsentation des Cent-Anteils.
	 */
	private String getFormatiertenCentAnteil()
	{
		String result = "";
		if (_centAnteil < 10)
		{
			result += "0";
		}
		result += _centAnteil;
		return result;
	}

	/**
	 * Vergleicht zwei Geldbeträge. this > g1 return 1 this < g1 return -1 this
	 * = g1 return 0
	 * 
	 * @param g1
	 *            Geldbetrag der mit this verglichen wird.
	 * @return this > g1 return 1 this < g1 return -1 this = g1 return 0
	 */
	public int compare(Geldbetrag g1)
	{
		if ((_euroAnteil * 100 + _centAnteil) > (g1.getEuroAnteil() * 100 + g1.getCentAnteil()))
		{
			return 1;
		}
		else if ((_euroAnteil * 100 + _centAnteil) < (g1.getEuroAnteil() * 100 + g1.getCentAnteil()))
		{
			return -1;
		}
		return 0;

	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime + _centAnteil;
		result = prime * result + _euroAnteil;
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		boolean result = false;
		if (obj instanceof Geldbetrag)
		{
			Geldbetrag other = (Geldbetrag) obj;
			result = (_centAnteil == other._centAnteil) && (_euroAnteil == other._euroAnteil);
		}
		return result;
	}

	/**
	 * Gibt diesen Geldbetrag in der Form "10,21" zurück.
	 * 
	 * @return result Geldbetrag in Form eines Strings
	 * 
	 * @ensure result != null
	 */
	@Override
	public String toString()
	{
		String result = _euroAnteil + "," + _centAnteil;

		if (_centAnteil == 0)
		{
			result = _euroAnteil + ",00";
		}
		else if (_centAnteil < 10)
		{
			result = _euroAnteil + ",0" + _centAnteil;
		}

		return result;
	}

	/**
	 * 
	 * Multipliziert einen Geldbetrag.
	 * 
	 * @param multiplikator
	 *            ein Multiplikator
	 * @return result das Produkt aus der multiplikation
	 * 
	 * @ensure result != null
	 */
	public Geldbetrag mulGeldbetraege(int multiplikator)
	{
		Geldbetrag result;
		int ergebnis;

		if (multiplikator > 0)
		{
			ergebnis = (_euroAnteil * 100) + _centAnteil;

			ergebnis = ergebnis * multiplikator;
		}
		else
		{
			ergebnis = 0;
		}

		result = new Geldbetrag(ergebnis);

		return result;
	}

	// private boolean Overflow(long a, long b )
	// {
	// BigInteger MAX = BigInteger.valueOf( Long.MAX_VALUE );
	// BigInteger bigA = BigInteger.valueOf( a );
	// BigInteger bigB = BigInteger.valueOf( b );
	//
	// return bigB.multiply( bigA ).compareTo( MAX ) <= 0;
	// }
	//

	/**
	 * Subtrahiert zwei Geldbeträge voneinander
	 * 
	 * @require g1 != null
	 * 
	 * @param g1
	 *            ein Geldbetrag
	 * @return result das Ergebnis der Subtraktion
	 * 
	 * @ensure result != null
	 */
	public Geldbetrag subGeldbetraege(Geldbetrag g1)
	{
		assert g1 != null : "Vorbedingung verletzt: g1 != null";

		Geldbetrag result;
		int ergebnis;

		ergebnis = _euroAnteil * 100 + _centAnteil;
		ergebnis -= ((g1.getEuroAnteil() * 100) + g1.getCentAnteil());

		if (ergebnis < 0)
		{
			ergebnis = 0;
		}

		result = new Geldbetrag(ergebnis);

		return result;
	}

	/**
	 * Addiert zwei Geldbeträge.
	 * 
	 * @require g1 != null
	 * 
	 * @param g1
	 * @return result Ergebniss der Addition
	 * 
	 * @ensure result != null
	 */
	public Geldbetrag addGeldbetraege(Geldbetrag g1)
	{
		assert g1 != null : "Vorbedingung verletzt: g1 != null";

		Geldbetrag result;
		int ergebnis;

		ergebnis = _euroAnteil * 100 + _centAnteil;
		ergebnis += ((g1.getEuroAnteil() * 100) + g1.getCentAnteil());

		if (ergebnis < 0)
		{
			ergebnis = 0;
		}

		result = new Geldbetrag(ergebnis);

		return result;
	}

	/**
	 * Wandelt eine ganze Zahl in ein Geldbetrag um.
	 * 
	 * @param zahl
	 * @return result Ein Geldbetrag
	 * 
	 * @ensure result != null
	 */
	public Geldbetrag IntToGeldbetrag(int zahl)
	{
		Geldbetrag result;

		if (zahl > 0)
		{
			result = new Geldbetrag(zahl);
		}
		else
		{
			result = new Geldbetrag(0);
		}
		return result;
	}

	/**
	 * Wandelt einen String in ein Geldbetrag um.
	 * 
	 * @require zahl != null
	 * 
	 * @param zahl
	 * @return result Ein Geldbetrag
	 * 
	 * @ensure result != null
	 */
	public Geldbetrag StringToGeldbetrag(String zahl)
	{
		assert zahl != null : "Vorbedingung verletzt: zahl != null";

		String euroAnteil = "";
		String centAnteil = "";
		Geldbetrag result;
		int ergebnis = 0;

		if (istGueltigeEingabe(zahl))
		{
			for (int i = 0; i < zahl.length(); i++)
			{
				if (zahl.charAt(i) == ',')
				{
					for (int j = 1; j < zahl.length() - i; j++)
					{
						centAnteil = centAnteil + zahl.charAt(i + j);
					}
					break;
				}
				else
				{
					euroAnteil = euroAnteil + zahl.charAt(i);
				}
			}
			ergebnis = (Integer.valueOf(euroAnteil) * 100) + Integer.valueOf(centAnteil);

		}
		result = new Geldbetrag(ergebnis);

		return result;

	}

	/**
	 * Gibt an ob ein String eine gültige Eingabe ist. Eine gültige Eingabe hat
	 * die Form zahl.zahlmit2ziffern z.B. 100.12
	 * 
	 * @require Integer.valueof(s) <= Integer.MAX_VALUE
	 * 
	 * @return result True wenn Eingabe gültig ist. False sonst.
	 * 
	 */
	public boolean istGueltigeEingabe(String s)
	{
		assert s != null : "Vorbedingung verletzt: s != null";
		boolean gueltig = true;
		boolean vorKomma = true;
		boolean ersteZahlNull;
		// 21474836,47.length() == 11(höchster eingebbarer
		// Geldbetrag(Integer.MAX_VALUE/100))
		if (s.length() >= 4 && s.length() <= 11)
		{
			ersteZahlNull = s.charAt(0) == '0';
			if (s.charAt(s.length() - 3) != ',' || ersteZahlNull && s.charAt(1) != ',')
			{
				gueltig = false;
			}

			for (int i = 0; i < s.length() && gueltig; i++)
			{
				if (s.charAt(i) == ',')
				{
					vorKomma = false;
				}

				if ((s.charAt(i) < '0' || s.charAt(i) > '9') && s.charAt(i) != ',')
				{
					gueltig = false;
				}
				else if (ersteZahlNull && vorKomma && s.charAt(i) != '0')
				{
					gueltig = false;
				}
				else if ((s.charAt(i) == ',') && (s.length() - i > 3))
				{
					gueltig = false;
				}
			}
		}
		else
		{
			gueltig = false;
		}

		return gueltig;

	}

}
