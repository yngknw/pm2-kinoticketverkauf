package de.hawhh.informatik.sml.kino.fachwerte;
import static org.junit.Assert.*;
//
import org.junit.Test;
public class GeldbetragTest
{
	@Test
	public void testToString()
	{
		Geldbetrag g1 = Geldbetrag.get(0);
		assertEquals(g1.toString(), "0,00");
		

		g1 = Geldbetrag.get(1);
		assertEquals(g1.toString(), "0,01");
		
		g1 = Geldbetrag.get(1000);
		assertEquals(g1.toString(), "10,00");

		
		g1 = Geldbetrag.get(Integer.MAX_VALUE);
		assertEquals(g1.toString(), "21474836,47");
		
		g1 = Geldbetrag.get(555);
		assertEquals(g1.toString(), "5,55");
		
		g1 = Geldbetrag.get(1111);
		assertEquals(g1.toString(), "11,11");
		
		g1 = Geldbetrag.get(123456);
		assertEquals(g1.toString(), "1234,56");
		
		g1 = Geldbetrag.get(180000);
		assertEquals(g1.toString(), "1800,00");

	}
	//21474836,47
	@Test
	public void testAddGeldbetraege()
	{
		Geldbetrag g1 = Geldbetrag.get(1000);
		Geldbetrag g2 = Geldbetrag.get(500);
		Geldbetrag res= Geldbetrag.get(1500);
		assertEquals(g1.addGeldbetraege(g2), res);
		
		g1 = Geldbetrag.get(Integer.MAX_VALUE/2 + 1);
		g2 = Geldbetrag.get(Integer.MAX_VALUE/2);
		res= Geldbetrag.get(Integer.MAX_VALUE);
		assertEquals(g1.addGeldbetraege(g2), res);
		
		g1 = Geldbetrag.get(0);
		g2 = Geldbetrag.get(0);
		res= Geldbetrag.get(0);
		assertEquals(g1.addGeldbetraege(g2), res);
		
		g1 = Geldbetrag.get(520);
		g2 = Geldbetrag.get(32222);
		res= Geldbetrag.get(32742);
		assertEquals(g1.addGeldbetraege(g2), res);
		
		g1 = Geldbetrag.get(87500);
		g2 = Geldbetrag.get(500);
		res= Geldbetrag.get(88000);
		assertEquals(g1.addGeldbetraege(g2), res);
	}
	
	@Test
	public void testSubGeldbetraege()
	{
		Geldbetrag g1 = Geldbetrag.get(1000);
		Geldbetrag g2 = Geldbetrag.get(500);
		Geldbetrag res= Geldbetrag.get(500);
		assertEquals(g1.subGeldbetraege(g2), res);
		
		g1 = Geldbetrag.get(Integer.MAX_VALUE);
		g2 = Geldbetrag.get(Integer.MAX_VALUE);
		res= Geldbetrag.get(0);
		assertEquals(g1.subGeldbetraege(g2), res);
		
		g1 = Geldbetrag.get(6000);
		g2 = Geldbetrag.get(420);
		res= Geldbetrag.get(5580);
		assertEquals(g1.subGeldbetraege(g2), res);

		g1 = Geldbetrag.get(12345);
		g2 = Geldbetrag.get(345);
		res= Geldbetrag.get(12000);
		assertEquals(g1.subGeldbetraege(g2), res);
		
		g1 = Geldbetrag.get(0);
		g2 = Geldbetrag.get(1);
		res= Geldbetrag.get(0);
		assertEquals(g1.subGeldbetraege(g2), res);
		
		g1 = Geldbetrag.get(Integer.MAX_VALUE);
		g2 = Geldbetrag.get(Integer.MAX_VALUE);
		res= Geldbetrag.get(0);
		assertEquals(g1.subGeldbetraege(g2), res);
	}
	
	@Test
	public void testMulGeldbetraege()
	{
		Geldbetrag g1 = Geldbetrag.get(1000);
		int zahl = 500;
		Geldbetrag res= Geldbetrag.get(500000);
		assertEquals(g1.mulGeldbetraege(zahl), res);
		
		g1 = Geldbetrag.get(0);
		zahl = 0;
		res= Geldbetrag.get(0);
		assertEquals(g1.mulGeldbetraege(zahl), res);
		
		g1 = Geldbetrag.get(1);
		zahl = -1;
		res= Geldbetrag.get(0);
		assertEquals(g1.mulGeldbetraege(zahl), res);
		
		g1 = Geldbetrag.get(500);
		zahl = 3;
		res= Geldbetrag.get(1500);
		assertEquals(g1.mulGeldbetraege(zahl), res);
		
		g1 = Geldbetrag.get(222);
		zahl = 5;
		res= Geldbetrag.get(1110);
		assertEquals(g1.mulGeldbetraege(zahl), res);
		
		g1 = Geldbetrag.get(Integer.MAX_VALUE);
		zahl = 0;
		res= Geldbetrag.get(0);
		assertEquals(g1.mulGeldbetraege(zahl), res);
		
//		g1 = new Geldbetrag(Integer.MAX_VALUE);
//		zahl = 101;
//		res= new Geldbetrag(0);
//		assertEquals(g1.mulGeldbetraege(zahl), res);
		
		g1 = Geldbetrag.get(30000);
		zahl = 9;
		res= Geldbetrag.get(270000);
		assertEquals(g1.mulGeldbetraege(zahl), res);
		
	}
	
	@Test
	public void testIntToGeldbetrag()
	{
		int zahl = 0;
		Geldbetrag g1 = Geldbetrag.get(0);
		Geldbetrag result = Geldbetrag.get(0);
		assertEquals(result, g1.IntToGeldbetrag(zahl));
		
		zahl = 568;
		g1 = Geldbetrag.get(0);
		result = Geldbetrag.get(568);
		assertEquals(result, g1.IntToGeldbetrag(zahl));
		
		zahl = -1;
		g1 = Geldbetrag.get(0);
		result = Geldbetrag.get(0);
		assertEquals(result, g1.IntToGeldbetrag(zahl));
		
		zahl = 100;
		g1 = Geldbetrag.get(0);
		result = Geldbetrag.get(100);
		assertEquals(result, g1.IntToGeldbetrag(zahl));
		
		zahl = Integer.MAX_VALUE;
		g1 = Geldbetrag.get(0);
		result = Geldbetrag.get(Integer.MAX_VALUE);
		assertEquals(result, g1.IntToGeldbetrag(zahl));
	}
	
	@Test
	public void testStringToGeldbetrag()
	{
		String zahl = "0";
		Geldbetrag g1 = Geldbetrag.get(0);
		Geldbetrag result = Geldbetrag.get(0);
		assertEquals(result, g1.StringToGeldbetrag(zahl));
		
		zahl = "5,68";
		g1 = Geldbetrag.get(0);
		result = Geldbetrag.get(568);
		assertEquals(result, g1.StringToGeldbetrag(zahl));
		
		zahl = "-0,01";
		g1 = Geldbetrag.get(0);
		result = Geldbetrag.get(0);
		assertEquals(result, g1.StringToGeldbetrag(zahl));
		
		zahl = "1,00";
		g1 = Geldbetrag.get(0);
		result = Geldbetrag.get(100);
		assertEquals(result, g1.StringToGeldbetrag(zahl));
		
		zahl = "21474836,47";
		g1 = Geldbetrag.get(0);
		result = Geldbetrag.get(Integer.MAX_VALUE);
		assertEquals(result, g1.StringToGeldbetrag(zahl));
	
	}
}

